import { NgModule } from '@angular/core';
import { FootersModule } from '@app/blocks/footers/footers.module';
import { ShellModule } from '@app/shell/shell.module';
import { SharedModule } from '@app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { HubModule } from '@app/blocks/services/hub.module';
import { ModalsModule } from '@app/blocks/modals/modals.module';

import { ApplyRoutingModule } from './apply-routing.module';
import { ApplyComponent } from './components/apply/apply.component';
import { ApplyApplyComponent } from './components/apply-apply/apply-apply.component';

@NgModule({
  declarations: [ApplyComponent, ApplyApplyComponent],
  imports: [
    FootersModule,
    ShellModule,
    ApplyRoutingModule,
    SharedModule,
    FormsModule,
    NgxMaskModule.forRoot(),
    HubModule,
    ModalsModule
  ]
})
export class ApplyModule { }
