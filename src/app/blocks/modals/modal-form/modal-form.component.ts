import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';
import { timer, Observable, Subscription } from 'rxjs';
import { AuthService } from '@app/blocks/services/auth.service';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import * as Fingerprint from 'fingerprintjs';

@Component({
  selector: 'dc-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss']
})
export class ModalFormComponent implements OnInit {
  smsModalId: string = `smsModalId`;
  smsApproveModalId: string = `smsApproveModalId`;
  date = new Date();
  timer$: Observable<number> = timer(0, 1000);
  timerSubscription: Subscription;
  counter: number;
  phone: string;
  errorMessage: string;
  timerTime = 120;
  @Input() size: string = 'xl';
  @Input() classes: string = '';

  constructor(
    public sanitizer: DomSanitizer,
    public ngxSmartModalService: NgxSmartModalService,
    private authService: AuthService,
    private router: Router,
    private gtmService: GoogleTagManagerService,
  ) {}
  
  ngOnInit() {
    console.log(this.router);
  }

  getCustomClass() {
    return `dialog-${this.size} ${this.classes}`;
  }

  async onSmsModalSubmit(form: NgForm) {
    const phone = form.value.phone;
    const tokenResponse = await this.authService.singleToken(phone);
    if (tokenResponse.ok) {
      this.ngxSmartModalService.setModalData(
        {
          phone,
          showAgain: false
        },
        this.smsApproveModalId,
        true
      );
      this.ngxSmartModalService.open(this.smsApproveModalId);
      this.ngxSmartModalService
        .getModal(this.smsApproveModalId)
        .onClose.subscribe(() => {
          this.timerSubscription.unsubscribe();
        });
      this.startTime(this.timerTime);
    }
  }

  async onSmsApproveModalSubmit(form: NgForm) {
    const token = form.value.smsCode;
    const data = this.ngxSmartModalService
      .getModal(this.smsApproveModalId)
      .getData();

    const signResponse = await this.authService.singleSign(data.phone, token);
    if (signResponse.ok) {
      this.ngxSmartModalService.getModal(this.smsApproveModalId).close();
      this.ngxSmartModalService.getModal(this.smsModalId).close();
      this.ngxSmartModalService.closeAll();
      const jsonResponse = await signResponse.json();
      this.authService.sign(jsonResponse.token);
      this.gtmService.pushTag({
        event: 'phoneConversationSubmit',
        orderId: new Fingerprint().get(),
      });
      this.gtmService.pushTag({
        event: 'interaction',
        category: 'phoneConversationSubmit',
        action: 'apply',
        label: new Fingerprint().get(),
        value: Date.now()
      });
      console.log(this.router);
      this.router.navigate(['/basvuru']);
    } else {
      const data = this.ngxSmartModalService.getModalData(
        this.smsApproveModalId
      );
      this.ngxSmartModalService.setModalData(
        {
          ...data,
          errorMessage: 'SMS kodu doğru degil.',
          showAgain: false
        },
        this.smsApproveModalId,
        true
      );
    }
  }

  async resend() {
    const data = this.ngxSmartModalService.getModalData(this.smsApproveModalId);
    const tokenResponse = await this.authService.singleToken(data.phone);
    if (tokenResponse.ok) {
      this.ngxSmartModalService.setModalData(
        {
          ...data,
          showAgain: false,
          errorMessage: false
        },
        this.smsApproveModalId,
        true
      );
      this.startTime(this.timerTime);
    }
  }

  startTime(time = 10) {
    const now = Date.now();

    this.timerSubscription = this.timer$.subscribe(() => {
      const elapsed = Math.floor((Date.now() - now) / 1000);
      this.counter = time - elapsed;
      const data = this.ngxSmartModalService.getModalData(
        this.smsApproveModalId
      );
      this.ngxSmartModalService.setModalData(
        {
          ...data,
          counter: this.counter
        },
        this.smsApproveModalId,
        true
      );

      if (elapsed >= time) {
        this.timerSubscription.unsubscribe();
        const data = this.ngxSmartModalService.getModalData(
          this.smsApproveModalId
        );
        this.ngxSmartModalService.setModalData(
          {
            ...data,
            counter: this.counter,
            errorMessage: false,
            showAgain: true
          },
          this.smsApproveModalId,
          true
        );
      }
    });
  }

  start() {
    var fingerprint = new Fingerprint().get();
    this.gtmService.pushTag({
      event: 'interaction',
      category: 'phoneConversationStart',
      action: 'apply',
      label: fingerprint,
      value: Date.now()
    });
    this.gtmService.pushTag({
      event: 'phoneConversationStart',
      orderId: fingerprint,
    });
    this.ngxSmartModalService.open(this.smsModalId);
  }
}
