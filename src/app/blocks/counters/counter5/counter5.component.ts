import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dc-counter5',
  templateUrl: './counter5.component.html',
  styleUrls: ['./counter5.component.scss']
})
export class Counter5Component implements OnInit {
  counters = [
    { icon: 'book', value: 1273, title: 'Değer Kaybı Geri Kazanımı' },
    { icon: 'book', value: 277, title: 'Başarılı Dava Dosyası Kazanımı' }
  ];
  constructor() {}

  ngOnInit() {}
}
