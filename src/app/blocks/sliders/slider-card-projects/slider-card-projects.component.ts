import { Component, OnInit, Input } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import {
  faInfoCircle,
  faLongArrowAltRight
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'dc-slider-card-projects',
  templateUrl: './slider-card-projects.component.html',
  styleUrls: ['./slider-card-projects.component.scss']
})
export class SliderCardProjectsComponent implements OnInit {
  infoCircle = faInfoCircle;
  longArrowAltRight = faLongArrowAltRight;

  @Input()
  liftUp: boolean = true;

  features = [
    {
      image: 'assets/img/cars/fiat-egea.jpg',
      name: 'FIAT EGEA SEDAN EASY 1.3',
      year: '2016',
      price: '5.700 TRY',
      meter: '16.000 KM',
      scale: 'BÜYÜK HASAR'
    },
    {
      image: 'assets/img/cars/ford-fiesta-1-5.jpg',
      name: 'FORD FİESTA 1.5 DİZEL',
      year: '2017',
      price: '4.200 TRY',
      meter: '86.000 KM',
      scale: 'ORTA HASAR'
    },
    {
      image: 'assets/img/cars/golf-1-6.jpg',
      name: 'GOLF 1.6 TDİ HIGHLINE',
      year: '2016',
      price: '4.450 TRY',
      meter: '154.000 KM',
      scale: 'BÜYÜK HASAR'
    },
    {
      image: 'assets/img/cars/mercedes-exc.jpg',
      name: 'MODEL Mercedes EXULVER Benz E180 1.6',
      year: '2018',
      price: '8.100 TRY',
      meter: '52.000 KM',
      scale: 'BÜYÜK HASAR'
    },
    {
      image: 'assets/img/cars/citroen-elysee.jpg',
      name: 'CITROEN ELYSEE 1.6 HDİ ',
      year: '2015',
      price: '3.750 TRY',
      meter: '107.000 KM',
      scale: 'BASİT HASAR'
    }
  ];

  config: SwiperConfigInterface = {
    slidesPerView: 4,
    autoplay: {
      delay: 3500,
      disableOnInteraction: false
    },
    loop: true,
    breakpoints: {
      '1024': { slidesPerView: 4, spaceBetween: 40 },
      '992': { slidesPerView: 2.5, spaceBetween: 15 },
      '768': { slidesPerView: 1.5, spaceBetween: 15 },
      '576': { slidesPerView: 1.5, spaceBetween: 10 }
    },
    navigation: {
      nextEl: '.features-nav-next',
      prevEl: '.features-nav-prev'
    },
    pagination: false
  };

  constructor() {}

  ngOnInit() {}
}
