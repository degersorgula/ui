#
# ---- Base Node ----
FROM node:12 AS base

RUN npm install npm -g
WORKDIR /usr/src
COPY package.json .

#
# ---- Dependencies ----
FROM base AS dependencies

RUN npm set progress=false
RUN npm install
RUN mv node_modules cache_node_modules

#
# ---- Release ----
FROM base AS release

COPY . .
COPY --from=dependencies /usr/src/cache_node_modules ./node_modules

RUN npm run build:ssr
CMD npm run serve:ssr
