import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SmartBusinessModule } from './pages/smart-business/smart-business.module';
import { FaqsModule } from './pages/faqs/faqs.module';
import { TermsModule } from './pages/terms/terms.module';
import { ApplyModule } from './pages/apply/apply.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => SmartBusinessModule
  },
  {
    path: 'sss',
    loadChildren: './pages/faqs/faqs.module#FaqsModule'
  },

  {
    path: 'kosullar',
    loadChildren: './pages/terms/terms.module#TermsModule'
  },
  {
    path: 'basvuru',
    loadChildren: './pages/apply/apply.module#ApplyModule'
  },

  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
