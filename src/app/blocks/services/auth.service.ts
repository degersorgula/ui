import { Injectable } from '@angular/core';
import BaseSerivce from './base.service';

@Injectable()
export class AuthService extends BaseSerivce {
  singleToken(phone: string) {
    return this.post('/auth/singleToken', {
      phone
    });
  }

  singleSign(phone: string, token: string) {
    return this.post('/auth/singleSign', {
      phone,
      token
    });
  }

  sign(token: string) {
    localStorage.setItem('token', token);
  }
}
