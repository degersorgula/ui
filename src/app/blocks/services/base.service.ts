import { environment } from '@env/environment';
import { Injectable } from '@angular/core';

@Injectable()
export default class BaseSerivce {
  defaultHeaders = {
    'Content-Type': 'application/json'
  };

  public async post(url, body) {
    const headers = Object.keys(this.defaultHeaders).reduce(
      (headerInstance, headerKey) => {
        headerInstance.append(headerKey, this.defaultHeaders[headerKey]);
        return headerInstance;
      },
      new Headers()
    );

    const token = localStorage.getItem('token');
    headers.append('Authorization', `Bearer ${token}`);

    var requestOptions = {
      method: 'POST',
      headers,
      body: JSON.stringify(body)
    };

    return fetch(
      `${environment.apiUrl}${environment.apiPrefix}${url}`,
      requestOptions
    );
  }
}
