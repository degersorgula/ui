import { ApplyService } from '@app/blocks/services/apply.service';
import { AuthService } from '@app/blocks/services/auth.service';
import { NgModule } from '@angular/core';

@NgModule({
  providers: [AuthService, ApplyService]
})
export class HubModule {}
