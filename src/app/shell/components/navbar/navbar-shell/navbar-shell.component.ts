import {
  Component,
  OnInit,
  Input,
  HostListener,
  EventEmitter,
  Output
} from '@angular/core';
import { faPhone } from '@fortawesome/free-solid-svg-icons';

const STICKY_POINT = 90;

@Component({
  selector: 'dc-navbar-shell',
  templateUrl: './navbar-shell.component.html',
  styleUrls: ['./navbar-shell.component.scss']
})
export class NavbarShellComponent implements OnInit {
  @Input()
  useOnlyDarkLogo: boolean;

  @Input()
  darkLinks: boolean;

  @Input()
  whiteMenu: boolean;

  @Input()
  darkLogo: boolean;

  @Input()
  navbarExpanded: boolean;

  navbarSticky: boolean;
  faPhone = faPhone;

  @Output() onNavbar = new EventEmitter();

  // @HostListener('window:scroll', ['$event'])
  handleScroll() {
    // const windowScroll = window.pageYOffset;
    // this.navbarSticky = false;
  }

  constructor() {}

  ngOnInit() {
  }

  toggleNavbar() {
    this.onNavbar.emit(this.navbarExpanded);
    this.navbarExpanded = !this.navbarExpanded;
  }

  call() {
    // window.location.href = 'tel:8505328111';
  }
}
