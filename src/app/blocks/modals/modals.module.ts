import { ModalFormComponent } from './modal-form/modal-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ModalVideoComponent } from './modal-video/modal-video.component';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';
import { HubModule } from '../services/hub.module';

@NgModule({
  declarations: [ModalVideoComponent, ModalFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxSmartModalModule.forChild(),
    NgxMaskModule.forRoot(),
    HubModule
  ],
  exports: [ModalVideoComponent, ModalFormComponent]
})
export class ModalsModule {}
