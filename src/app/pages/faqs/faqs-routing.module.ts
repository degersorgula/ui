import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FaqsComponent } from './components/faqs/faqs.component';
import { FaqsCommonComponent } from './components/faqs-common/faqs-common.component';

const routes: Routes = [
  {
    path: '',
    component: FaqsComponent,
    children: [
      { path: '', redirectTo: 'common', pathMatch: 'full' },
      { path: 'common', component: FaqsCommonComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaqsRoutingModule {}
