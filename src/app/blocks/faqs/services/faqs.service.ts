import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FaqsService {
  constructor() {}

  getFaqs(): Observable<any> {
    return of([
      {
        question: 'Değer Kaybı Başvuru Süreci',
        answer:
          'İnternet sitemiz üzerinden gerekli evrakları ve istekleri tamamlarsınız. Alanında deneyimli ekibimiz süreci yönetmeye başlar.'
      },
      {
        question: 'Başvuru Reddi ve Sigorta Tahkim',
        answer:
          'Sigorta şirketinizin değer kaybı geri kazanım talebinizi kabul etmemesi durumunda, yine bu alanda deneyimli avukatlarımız sizin dava sürecinizi yönetmek için hazır olacaklar.'
      },
      {
        question: 'Nasıl Paramı Alırım ?',
        answer:
          'Sizlere bütün süreçlerle ilgili şeffaf olarak bilgilendirme yapılıp, hasar değer kaybı başvurusu sonuçlandığı gün geri kazanımınız kendi hesabınıza transfer edilecektir.'
      }
    ]);
  }
}
