import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'dc-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.scss']
})
export class ApplyComponent implements OnInit {
  constructor(
    private meta : Meta, 
    private title : Title
  ) {}

  ngOnInit() {
    this.meta.addTags([{
      name: 'description',
      content: 'Kaza sonrası Sigortanızdan (hasar dışı) Maddi ve manevi tazminat alabilmek için başvurun.'
    }]);
    this.title.setTitle('Başvuru - Araç Değer Kaybı Sorgulama')
  }
}
