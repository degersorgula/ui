import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';


@Component({
  selector: 'dc-smart-business',
  templateUrl: './smart-business.component.html',
  styleUrls: ['./smart-business.component.scss']
})
export class SmartBusinessComponent implements OnInit {
  constructor(
    private meta : Meta, 
    private title : Title
  ) {}

  ngOnInit() {
    this.meta.addTags([{
      name: 'description',
      content: 'Kaza sonrası Sigortanızdan (hasar dışı) Maddi ve manevi tazminat alabilmek için başvurun.'
    }]);
    this.title.setTitle('Araç Değer Kaybı Sorgulama')
  }
}
