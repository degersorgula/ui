import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyApplyComponent } from './apply-apply.component';

describe('ApplyApplyComponent', () => {
  let component: ApplyApplyComponent;
  let fixture: ComponentFixture<ApplyApplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplyApplyComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyApplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
