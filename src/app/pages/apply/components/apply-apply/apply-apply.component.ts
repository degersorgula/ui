import { ApplyService } from '@app/blocks/services/apply.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GoogleTagManagerService } from 'angular-google-tag-manager';
import * as Fingerprint from 'fingerprintjs';

@Component({
  selector: 'dc-apply-apply',
  templateUrl: './apply-apply.component.html',
  styleUrls: ['./apply-apply.component.scss']
})
export class ApplyApplyComponent implements OnInit {
  hideForm = false;
  authError = false;
  falbackError = false;
  showSuccess = false;

  // fullname = 'Seyit Ali';
  // tckno = '25760039978';
  // year = '2019';
  // brand = 'Opel';
  // model = 'Astra 2019';
  // crashDate = '25/02/2019';
  // plaque = '34 ALI 1010';

  fullname = '';
  tckno = '';
  year = '';
  brand = '';
  model = '';
  crashDate = '';
  plaque = '';
  constructor(
    private applyService: ApplyService,
    private gtmService: GoogleTagManagerService,
  ) {}

  ngOnInit() {}

  async onSmsModalSubmit(form: NgForm) {
    const fullname = form.value.fullname;
    const tckno = form.value.tckno;
    const brand = form.value.brand;
    const year = form.value.year;
    const model = form.value.model;
    const crashDate = form.value.crashDate;
    const plaque = form.value.plaque;

    const applyResponse = await this.applyService.apply(
      fullname,
      tckno,
      brand,
      year,
      model,
      crashDate,
      plaque
    );

    if (applyResponse.ok) {
      this.gtmService.pushTag({
        event: 'formConversationSubmit',
        orderId: new Fingerprint().get(),
      });
      this.gtmService.pushTag({
        event: 'interaction',
        category: 'formConversationSubmit',
        action: 'apply',
        label: new Fingerprint().get(),
        value: Date.now()
      });

      this.hideForm = true;
      this.showSuccess = true;
    } else if (applyResponse.status === 401) {
      this.authError = true;
      this.hideForm = true;
    } else if (applyResponse.status === 201) {
      this.falbackError = true;
      this.hideForm = true;
    }
  }
}
