import { Component, OnInit, Input } from '@angular/core';
import { faSignInAlt, faPhone } from '@fortawesome/free-solid-svg-icons';
import { isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'dc-navbar-default',
  templateUrl: './navbar-default.component.html',
  styleUrls: ['./navbar-default.component.scss']
})
export class NavbarDefaultComponent implements OnInit {
  @Input()
  useOnlyDarkLogo: boolean;

  @Input()
  darkLogo: boolean;

  @Input()
  whiteMenu: boolean;

  @Input()
  darkLinks: boolean;

  @Input()
  position: string;

  navbarExpanded: boolean;

  signInAlt = faSignInAlt;
  faPhone = faPhone;

  constructor() {}

  ngOnInit() {}

  isRightPositioned() {
    return this.position === 'right';
  }

  test(navbarExpanded) {
    this.navbarExpanded = navbarExpanded;
  }

  scrollHow() {
    this.navbarExpanded = false;
    setTimeout(() => {
      if (isPlatformBrowser) {
        window['nasil'].scrollIntoView({ behavior: 'smooth' });
      }
    }, 1000);
  }
}
