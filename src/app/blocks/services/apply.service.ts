import { Injectable } from '@angular/core';
import BaseSerivce from './base.service';

@Injectable()
export class ApplyService extends BaseSerivce {
  apply(
    fullname: string,
    tckno: string,
    brand: string,
    year: string,
    model: string,
    crashDate: string,
    plaque
  ) {
    return this.post('/entry/new', {
      fullname,
      tckno,
      brand,
      year,
      model,
      crashDate,
      plaque
    });
  }
}
